import React, { Component } from "react";
import { Text, StyleSheet, View, SafeAreaView, Platform, Image, StatusBar, TextInput, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback } from "react-native";
import styles from './styles';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import LinearGradient from "react-native-linear-gradient";
import OptionTab from '../../components/OptionTab';

const onImage = require("../../images/on.png");
const offImage = require("../../images/off.png");

class PayBill extends Component {
  constructor(props){
    super(props);
    this.state = {
      text: null,
      option1: true,
      option2: false
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => {
        Keyboard.dismiss();
      }}>
        <SafeAreaView
          forceInset={{ bottom: "always" }}
          style={styles.pageContainer}
        >
          <StatusBar backgroundColor="#4db6ac" barStyle="light-content" />

          <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "height" : "padding"}
            enabled
            style={{
              backgroundColor: "#f4f4f4",
              flex: 1
            }}
          >
            <LinearGradient
              colors={["#00d1c1", "#00d1dc"]}
              style={styles.bgLayer}
            />
            <View style={styles.mainLayer}>
              {/* profile and cancel block */}
              <View style={styles.profileRow}>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Text style={styles.profileName}>{"Roshan Sam"}</Text>
                    <Text style={styles.numberText}>{"9870763024"}</Text>
                  </View>
                  <View style={styles.changeButton}>
                    <Text style={styles.changeText}>
                      {"Change".toUpperCase()}
                    </Text>
                  </View>
                </View>

                <FontAwesome5 name="times" size={20} color="#fff" />
              </View>

              {/* input enter block */}
              <View style={styles.inputBlock}>
                <Image
                  source={require("../../images/rupee.png")}
                  style={styles.dollarStyle}
                />
                <TextInput
                  autoFocus={true}
                  multiline={false}
                  keyboardType={"numeric"}
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  placeholderTextColor="#99ffffff"
                  placeholder={"Enter Bill Amount"}
                  value={this.state.text}
                  onChangeText={text => this.setState({ text })}
                />
              </View>

              {/* option select block */}
              <View style={styles.payOptionBlock}>
                <Text style={styles.payUsingText}>Pay using</Text>
                <View style={styles.borderLine} />
                <View style={styles.options}>
                  {/* on option */}
                  <OptionTab
                    onPressFunc={() => this.setState({ option1: true, option2: false })}
                    sourceImage={this.state.option1 ? onImage : offImage}
                    titleText={"Simpl: ₹2000 available"}
                    supportText={"₹1922 will be charged to your account"}
                  />

                  {/* off option */}
                  <OptionTab
                    onPressFunc={() => this.setState({ option2: true, option1: false })}
                    sourceImage={this.state.option2 ? onImage : offImage}
                    titleText={"UPI"}
                    supportText={"A payment request will be sent to your VPA"}
                  />
                </View>
              </View>
            </View>

            {/* footer */}
            <TouchableWithoutFeedback
              onPress={() => {
                alert("You have entered " + this.state.text);
                Keyboard.dismiss();
              }}
            >
              <LinearGradient
                colors={["#00d1c1", "#00d1dc"]}
                style={styles.footerBlock}
              >
                <FontAwesome5 name="lock" size={15} color="#fff" />
                <Text style={styles.payBillText}>{"PAY BILL"}</Text>
              </LinearGradient>
            </TouchableWithoutFeedback>

          </KeyboardAvoidingView>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    );
  }
}

export default PayBill;
