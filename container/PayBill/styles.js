import { StyleSheet, Dimensions } from "react-native";
let { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    backgroundColor: "#00d1c1"
  },
  bgLayer: {
    width: width,
    height: 250
  },
  mainLayer: {
    position: "absolute",
    padding: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    backgroundColor: "transparent",
    flex: 1
  },
  profileRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 5
  },
  profileName: {
    color: "#ffffff",
    fontSize: 18,
    fontFamily: "SourceSansPro-Semibold"
  },
  numberText: {
    color: "#ffffff",
    fontSize: 13,
    lineHeight: 16,
    fontFamily: "SourceSansPro-Regular"
  },
  changeButton: {
    marginLeft: 16,
    backgroundColor: "#fff",
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  changeText: {
    letterSpacing: 0.92,
    textAlign: "center",
    color: "#00d1c1",
    fontSize: 12,
    fontFamily: "SourceSansPro-Bold"
  },
  inputBlock: {
    paddingHorizontal: 16,
    alignItems: "center",
    marginVertical: 20,
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#4cffffff",
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: "#0c000000"
  },
  textInput: {
    fontSize: 24,
    fontFamily: "SourceSansPro-Bold",
    height: 50,
    flex: 1,
    lineHeight: 25,
    paddingLeft: 5,
    color: "#99ffffff",
    borderRadius: 2,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0c000000",
  },
  dollarStyle: {
    width: 15,
    height: 20,
    marginRight: 10,
  },
  payOptionBlock: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0
    },
    elevation: 5,
    shadowRadius: 4,
    shadowOpacity: 0.1,
    backgroundColor: "#fff",
    paddingTop: 16,
    borderRadius: 4,
    zIndex: 1
  },
  payUsingText: {
    color: "#77898a",
    fontSize: 13,
    fontFamily: "SourceSansPro-Regular",
    lineHeight: 16,
    paddingHorizontal: 16,
    paddingBottom: 16
  },
  borderLine: {
    borderColor: "#e6ebe9",
    borderWidth: 0.5
  },
  options: {
    paddingVertical: 16
  },
  footerBlock: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    padding: 16
  },
  payBillText: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "SourceSansPro-Semibold",
    textAlign: "center",
    paddingLeft: 10
  }
});

export default styles;
