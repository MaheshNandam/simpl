import { StyleSheet, Dimensions } from "react-native";
let { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  insideBlock: {
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingBottom: 16
  },
  titleText: {
    color: "#344647",
    fontSize: 15,
    fontFamily: "SourceSansPro-Semibold",
    paddingBottom: 1
  },
  supportText: {
    color: "#77898a",
    fontSize: 13,
    fontFamily: "SourceSansPro-Regular"
  },
  optionImage: {
    width: 18,
    height: 20,
    marginRight: 16
  }
});

export default styles;
