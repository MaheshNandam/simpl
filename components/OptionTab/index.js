import React, { Component } from "react";
import { Text, View, TouchableWithoutFeedback, Image } from "react-native";
import styles from "./styles";

export default class OptionTab extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={this.props.onPressFunc}>
        <View style={styles.insideBlock}>
          <Image
            source={this.props.sourceImage}
            style={styles.optionImage}
          />
          <View>
            <Text style={styles.titleText}>{this.props.titleText}</Text>
            <Text style={styles.supportText}>{this.props.supportText}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
