/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import PayBill from './container/PayBill';

export default class App extends Component {
  render() {
    return (
      <PayBill />
    );
  }
}
